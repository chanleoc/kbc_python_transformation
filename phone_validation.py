"__author__ = 'Leo Chan'"
"__credits__ = 'Keboola 2017'"

"""
Phone Number Validation
Python 3 environment 
"""

import pip
pip.main(['install', '--disable-pip-version-check', '--no-cache-dir', 'phonenumbers'])

import phonenumbers
import pandas as pd

#########################################################################################################
###############     INPUT VARIABLES     #################################################################

input_file_name = "numbers.csv"
output_file_name = "output.csv"

time_column = "phone_numbers"

# Has to be specified, Ex: HK, CA, US, CZ
region = "CA"

#########################################################################################################
#########################################################################################################


DEFAULT_INPUT_FILE = "/data/in/tables/"
DEFAULT_OUTPUT_FILE = "/data/out/tables/"

input_file = DEFAULT_INPUT_FILE+input_file_name
output_file = DEFAULT_OUTPUT_FILE+output_file_name
new_column_name = time_column+"_corrected"

data = pd.read_csv(input_file, dtype=str)
data[new_column_name]=""

suggested_value = []
for i in data[time_column]:
    x = phonenumbers.parse(i,region)
    print(x)
    if phonenumbers.is_possible_number(x):
        suggested_value.append(phonenumbers.format_number(x,phonenumbers.PhoneNumberFormat.NATIONAL))
    else:
        suggested_value.append("Invalid Number")
data[new_column_name]=suggested_value

data.to_csv(output_file,index=False)

print("Done.")



